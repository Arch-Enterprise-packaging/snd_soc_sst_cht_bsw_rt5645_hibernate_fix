#!/bin/sh

# reset snd_soc_sst_cht_bsw_rt5645 after hibernate

# put this file into /lib/systemd/system-sleep/

case $1 in
  pre)
      :
  ;;
  post)
    killall pulseaudio
    rmmod snd_soc_sst_cht_bsw_rt5645
    rmmod snd_soc_rt5645
    modprobe snd_soc_rt5645
    modprobe snd_soc_sst_cht_bsw_rt5645
    ;;
esac
